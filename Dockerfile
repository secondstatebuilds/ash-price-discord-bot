FROM oven/bun:latest

COPY package.json ./
COPY bun.lockb ./
COPY .env ./
COPY bot.js ./

RUN bun install

CMD ["bun", "bot.js"]