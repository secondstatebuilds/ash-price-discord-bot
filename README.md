## ASH Price Discord Bot

This bot displays the price of $ASH in both $ and Ξ on the sidebar of Discord. 

The bot can be invited using this link: [✖️Invite Me✖️](https://discord.com/oauth2/authorize?client_id=926943946914005003&permissions=0&scope=bot "✖️Invite Me✖️")
## Self host


Create a new bot over at https://discord.com/developers/applications , then grab the DISCORD_BOT_TOKEN and put in in the .env file. 
See .env.example for other required variables.

Docker:

```bash
git clone https://gitlab.com/secondstatebuilds/ash-price-discord-bot.git
cd ash-price-discord-bot
cp .env.example .env
# edit .env with your variables
docker compose build
docker compose up -d
```


Or just run via bun(or node):
```bash
git clone https://gitlab.com/secondstatebuilds/ash-price-discord-bot.git
cd ash-price-discord-bot
cp .env.example .env
# edit .env with your variables
bun install
bun ./bot.js
```


### Note on Fetching ASH Price

Since most services have ended their free tier for hosting ASH prices, you have two options to fetch the price:

#### 1. Using Subgraph (API key required)

Endpoint: `https://gateway-arbitrum.network.thegraph.com/api/${apiKey}/subgraphs/id/5zvR82QoaXYFyDEKLZ9t6v9adgnptxYpKpSbxtgVENFV`

Query body:

```js
const QUERY_BODY = {
    query: `{
    token(id:"0x64d91f12ece7362f91a6f8e7940cd55f05060b92") {
        symbol
        derivedETH
    }
    bundle(id: "1" ) {   ethPriceUSD }
  }`
};
```

#### 2. Using CoinGecko API

Endpoint: `https://api.coingecko.com/api/v3/coins/ash/tickers?exchange_ids=uniswap_v3`

The bot expects a response of(due to the way subgraph works and most services are relying on this format for my use case): 

```js
...
    return {
        data: {
            token: {
                symbol: 'ASH',
                derivedETH: ashPrice.last.toString()
            },
            bundle: {
                ethPriceUSD: ethPrice.usd.toString() // comes from https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=usd
            }
        }
    };
```

