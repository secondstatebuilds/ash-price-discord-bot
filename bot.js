import dotenv from "dotenv";
import { Client, GatewayIntentBits, Events, ActivityType } from "discord.js";

dotenv.config();

const client = new Client({
  intents: [GatewayIntentBits.GuildPresences, GatewayIntentBits.GuildMessages],
});

client.login(process.env.DISCORD_BOT_TOKEN);
const url = process.env.ASH_PRICE_ENDPOINT;
const cacheFile = "price_cache.json";
const errorCacheFile = "error_cache.json";

// listen for the client to be ready
client.once(Events.ClientReady, (c) => {
  console.log(`Ready! Logged in as ${c.user.tag}`);
  // Call setAct immediately upon startup
  setAct();
});

async function getPrice() {
  try {
    const res = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        "wcb-auth-key": process.env.WCB_AUTH_KEY,
      },
    });
    const {
      data: { bundle, token },
    } = await res.json();
    const priceData = [
      (token.derivedETH * bundle.ethPriceUSD).toFixed(2),
      parseInt(bundle.ethPriceUSD, 10),
      token.derivedETH.toString().substring(0, 6),
    ];

    // Cache the price data using Bun.write
    await Bun.write(cacheFile, JSON.stringify(priceData));

    // If successful, remove the error cache file if it exists
    const errorCacheExists = await Bun.file(errorCacheFile).exists();
    if (errorCacheExists) {
      await Bun.write(errorCacheFile, "");
    }

    return priceData;
  } catch (error) {
    console.error("Error fetching price, using cached data:", error);
    await sendUniqueAlert(
      "Error fetching price, using cached data",
      error.message
    );
    try {
      const cachedData = await Bun.file(cacheFile).text();
      return JSON.parse(cachedData);
    } catch (cacheError) {
      console.error("Error reading cache:", cacheError);
      await sendUniqueAlert("Error reading cache", cacheError.message);
      return ["0.00", 0, "0.0000"];
    }
  }
}

async function setAct() {
  const priceData = await getPrice();
  client.user.setPresence({
    activities: [
      {
        name: `✖️$${priceData[0]} | Ξ${priceData[2]}`,
        type: ActivityType.Watching,
      },
    ],
    status: "online",
  });
}

// Update price every 3 minutes (180000 milliseconds)
setInterval(setAct, 180000);

async function sendUniqueAlert(errorType, errorMessage) {
  try {
    const errorCacheExists = await Bun.file(errorCacheFile).exists();
    let errorData = {};
    if (errorCacheExists) {
      const errorCacheContent = await Bun.file(errorCacheFile).text();
      errorData = errorCacheContent ? JSON.parse(errorCacheContent) : {};
    }
    if (!errorData[errorType]) {
      errorData[errorType] = true;
      await Bun.write(errorCacheFile, JSON.stringify(errorData));
      await sendTelegramAlert(`${errorType}: ${errorMessage}`);
    }
  } catch (error) {
    console.error("Error handling unique alert:", error);
  }
}

async function sendTelegramAlert(text) {
  const headers = {
    "Content-Type": "application/json",
  };
  const body = {
    chat_id: process.env.GROUP_ID,
    text: text,
  };
  try {
    await fetch(
      `https://api.telegram.org/bot${process.env.ALERT_TOKEN}/sendMessage`,
      {
        method: "POST",
        headers,
        body: JSON.stringify(body),
      }
    );
    console.log("Telegram alert sent successfully");
  } catch (error) {
    console.error("Error sending Telegram alert:", error);
  }
}
